/** @type {import('next').NextConfig} */
const nextConfig = {
  images: {
    // remotePatterns: [
    //   {
    //     protocol: "http",
    //     hostname: "**",
    //   },
    // ],
    domains: ["openweathermap.org"],
  },
};

export default nextConfig;
