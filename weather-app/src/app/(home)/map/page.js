"use client";
import { useState, useRef, useEffect } from "react";
import Map, {
  NavigationControl,
  Marker,
  Popup,
  FullscreenControl,
  Source,
  Layer,
} from "react-map-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import { FaBoxOpen, FaPlaneArrival } from "react-icons/fa";
import { FaBox, FaPlaneUp } from "react-icons/fa6";
import { TbDrone } from "react-icons/tb";
import { IoIosClose } from "react-icons/io";
import { MdFlightTakeoff, MdHome } from "react-icons/md";
import { TiWeatherCloudy } from "react-icons/ti";
import Places from "@/components/map/place";
import "@/app/(home)/map/map.css"
import { lineStyle, geojson, endPoint, endPointStyle } from "@/constants/mapbox";
import Navigation from "@/components/map/navigation";
import toast, { Toaster } from 'react-hot-toast';

export default function MapBoxGL() {
  const mapboxToken = process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN;
  const [selectedMarker, setSelectedMarker] = useState(null);
  const [start, setStart] = useState([105.804817, 21.028511]);
  const [end, setEnd] = useState([105.804817, 21.5]);
  const [coords, setCoords] = useState([]);
  const [steps, setSteps] = useState([]);
  const [droneStatus, setDroneStatus] = useState('');
  const [isFly, setIsFly] = useState(false);
  const mapRef = useRef(null);

  const handleBtnClick = (status) => {
    setDroneStatus(status);
    if (status === 'takeOff') {
      toast.promise(saveSettings({ droneStatus: status }), {
        loading: 'Taking off...',
        success: <p>Drone is taking off</p>,
        error: <p>Failed to take off</p>,
      }).finally(() => { setDroneStatus('idle') });
    } else if (status === 'back') {
      setEnd(start);
      toast.promise(saveSettings({ droneStatus: status }), {
        loading: 'Get back...',
        success: <p>Welcome back Drone A!</p>,
        error: <p>Failed to get back</p>,
      }).finally(() => { setDroneStatus('idle') });
    } else if (status === 'flying') {
      setIsFly(true);
    } if (status === 'landing') {
      toast.promise(saveSettings({ droneStatus: status }), {
        loading: 'Landing...',
        success: <p>Landing successfully</p>,
        error: <p>Failed to land</p>,
      }).finally(() => { setDroneStatus('idle') });
    }
  };

  const saveSettings = (settings) => {
    return new Promise((resolve) => {
      setTimeout(() => {
        console.log('Settings saved:', settings);
        resolve('Settings saved');
      }, 2000);
    });
  };

  const isDisableBtn = (buttonStatus) => {
    if (droneStatus === 'takeOff' || droneStatus === 'back') {
      return true;
    }
    if ((droneStatus === 'flying' && buttonStatus === 'takeOff')
      || (droneStatus === 'landing' && (buttonStatus === 'flying' || buttonStatus === 'takeOff'))) {
      return true;
    }
    return false;
  }

  useEffect(() => {
    getDirection();
  }, [end]);

  const getDirection = async () => {
    try {
      const res = await fetch(
        `https://api.mapbox.com/directions/v5/mapbox/driving/${start[0]},${start[1]};${end[0]},${end[1]}?steps=true&geometries=geojson&access_token=${mapboxToken}`
      );
      const data = await res.json();
      const coords = data.routes[0].geometry.coordinates;
      setCoords(coords);
      const steps = data.routes[0].legs[0].steps;
      setSteps(steps);
    } catch (error) {
      console.log("Error fetching data: ", error);
    }
  };

  const handleClick = (e) => {
    const newEnd = e.lngLat;
    setEnd([newEnd.lng, newEnd.lat]);
  };

  const handleDragEnd = (e) => {
    const newEnd = e.lngLat;
    setEnd([newEnd.lng, newEnd.lat]);
  };

  const handleMarkerClick = (markerId) => {
    setSelectedMarker((prevMarker) => {
      if (prevMarker === markerId) {
        return null;
      }
      return markerId;
    });
  };

  return (
    <main className="max-w-full h-screen relative">
      <Map
        // onClick={handleClick}
        ref={mapRef}
        mapboxAccessToken={mapboxToken}
        mapStyle="mapbox://styles/mapbox/streets-v12"
        style="w-full h-full"
        initialViewState={{
          latitude: 21.028511,
          longitude: 105.804817,
          zoom: 7.5,
        }}
        maxZoom={20}
        minZoom={3}
      >
        <div><Toaster /></div>
        <Source id="routeSource" type="geojson" data={geojson(coords)}>
          <Layer {...lineStyle} />
        </Source>

        <Source id="endPoint" type="geojson" data={endPoint(end)}>
          <Layer {...endPointStyle(end)} />
        </Source>

        {/* Extension */}
        <FullscreenControl position="bottom-right" />
        <NavigationControl position="bottom-right" />

        {/* Marker for start point (box) */}
        <Marker longitude={start[0]}
          latitude={start[1]}
          onClick={(e) => handleMarkerClick("start", e)}
        >
          {selectedMarker === "start" ? (<FaBoxOpen color="gray" size={30} />) : (<FaBox color="gray" size={30} />)}
        </Marker>

        {selectedMarker === "start" && (
          <Popup
            latitude={start[1]}
            longitude={start[0]}
            closeButton={false}
            closeOnClick={false}
            style={{ paddingBottom: "10px " }}
          >
            <div className="p-4 w-35 bg-slate-400">
              <div className="flex justify-between items-center">
                <h1 className="text-xl font-semibold text-black">Site A</h1>
                <IoIosClose className="cursor-pointer text-black hover:text-white absolute
                  right-0 top-0 hover:bg-red-600"
                  size={20}
                  onClick={() => handleMarkerClick()}
                />
              </div>
              <div className="flex flex-col gap-2">
                <button
                  className="bg-blue-500 hover:bg-blue-600 text-white py-2 px-4
                   rounded-md focus:bg-gray-600"
                >
                  Option 1
                </button>
                <button
                  className="bg-green-500 hover:bg-green-600 text-white py-2 px-4 rounded-md focus:bg-gray-600"
                >
                  Option 2
                </button>
                <button
                  className="bg-red-500 hover:bg-red-600 text-white py-2 px-4 rounded-md focus:bg-gray-600"
                >
                  Option 3
                </button>
              </div>
            </div>
          </Popup>
        )}

        {/* Marker for end point (drone) */}
        <Marker longitude={end[0]} latitude={end[1]}
          offsetLeft={-12}
          offsetTop={-24}
          draggable
          onDragEnd={handleDragEnd}
          onClick={(e) => handleMarkerClick("end", e)}>
          <TbDrone color="black" size={30} />
        </Marker>

        {selectedMarker === "end" && (
          <Popup
            latitude={end[1]}
            longitude={end[0]}
            closeButton={false}
            closeOnClick={false}
          >
            {isFly ? (
              <div className="flex items-end justify-center text-center sm:block sm:p-0">
                <div className="inline-block align-bottom bg-white rounded-lg text-left">
                  <div className="bg-white pt-5 pb-4 px-1">
                    <div className="mt-3 text-center">
                      <h3 className="text-lg leading-6 font-bold text-gray-900">
                        Where do you want to go?
                      </h3>
                      <div className="mt-2">
                        <Places setEnd={setEnd} />
                      </div>
                    </div>
                  </div>
                  <div className="bg-gray-50 px-4 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
                    <button type="button" className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:w-auto sm:text-sm" onClick={() => setIsFly(false)}>  
                      Cancel
                    </button>
                  </div>
                </div>
              </div>) : (<div className="w-80 bg-slate-400">
                <div className="flex justify-center items-center mb-2 relative">
                  <h1 className="text-xl font-semibold text-black pt-2">Drone A</h1>
                  <IoIosClose className="cursor-pointer text-black hover:text-white absolute
                  right-0 top-0 hover:bg-red-600"
                    size={20}
                    onClick={(e) => handleMarkerClick()}
                  />
                </div>
                <div className="w-50 p-4 pt-0">
                  <h1 className="text-black font-semibold text-[14px] my-1">Standard Control</h1>
                  <div className="grid grid-cols-2 gap-1 mb-2">
                    <button
                      className="flex justify-between items-center bg-blue-500 hover:bg-blue-600 text-white py-3 px-3 rounded-3xl"
                      onClick={() => window.location.href = "/weather"}
                    >
                      <TiWeatherCloudy className="text-current" size={20} />
                      <p className="text-current font-semibold text-[12px]">Current weather</p>
                    </button>
                  </div>
                  <div className="">
                    <h1 className="text-black font-semibold text-[14px] my-1">Remote Control</h1>
                    <div className="grid grid-cols-2 gap-1">
                      <button
                        className="flex justify-center items-center gap-x-2 bg-red-500
                      hover:bg-red-600 text-black py-2 px-4 rounded-md hover:text-white"
                        onClick={() => handleBtnClick("takeOff")}
                        disabled={isDisableBtn("takeOff")}
                      >
                        <MdFlightTakeoff className="text-current" size={30} />
                        <p className="text-current font-semibold">Take Off</p>
                      </button>

                      <button
                        className="flex justify-center items-center gap-x-2 bg-red-500
                      hover:bg-red-600 text-black py-2 px-4 rounded-md hover:text-white"
                        onClick={() => handleBtnClick("landing")}
                        disabled={isDisableBtn("landing")}
                      >
                        <FaPlaneArrival className="text-current" size={27} />
                        <p className="text-current font-semibold">Landing</p>
                      </button>

                      <button
                        className="flex justify-center items-center gap-x-2 bg-red-500
                      hover:bg-red-600 text-black py-2 px-4 rounded-md hover:text-white"
                        onClick={() => handleBtnClick("flying")}
                        disabled={isDisableBtn("flying")}
                      >
                        <FaPlaneUp className="text-current" size={27} />
                        <p className="text-current font-semibold">Flying</p>
                      </button>
                      <button
                        className="flex justify-center items-center gap-x-2 bg-red-500
                      hover:bg-red-600 text-black py-2 px-4 rounded-md hover:text-white
                      focus:text-white"
                        onClick={() => handleBtnClick("back")}
                        disabled={isDisableBtn("back")}
                      >
                        <MdHome className="text-current" size={30} />
                        <p className="text-current font-semibold">Back home</p>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </Popup>
        )}
        <Navigation steps={steps} />
        {/* <Places setEnd={setEnd} /> */}
      </Map>

    </main >
  );
}
