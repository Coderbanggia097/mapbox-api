"use client";
import Image from "next/image";
import Map from "@/components/map/map";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getCurrentData,
  getForecastData,
  getPolutionData,
} from "@/features/WeatherSlice";
import WeatherDetails from "@/components/weather/weatherDetails";
import WeatherPollutions from "@/components/weather/weatherPollution";
import WeatherForecast from "@/components/weather/weatherForecast";
import WeatherMain from "@/components/weather/weatherMain";
import { SphereSpinner } from "react-spinners-kit";
import toast, { ToastContainer } from "react-hot-toast";

export default function Home() {
  const dispatch = useDispatch();

  const {
    citySearchLoading,
    forecastLoading,
  } = useSelector((state) => state.weather);

  const [position, setPosition] = useState(["", ""]);
  const [isError, setIsError] = useState(false);

  // Loading
  const [loadings, setLoadings] = useState(true);
  const allLoadings = [citySearchLoading, forecastLoading];
  useEffect(() => {
    const isLoading = citySearchLoading || forecastLoading;
    setLoadings(isLoading);
  }, [citySearchLoading, forecastLoading]);

  const [city, setCity] = useState("Hanoi");
  const [unit, setUnit] = useState("metric");

  const fetchData = () => {
    dispatch(
      getCurrentData({
        city,
        unit,
      })
    ).then((res) => {
      if (res.payload && res.payload.data) {
        const item = res.payload.data.coord;
        setPosition([item.lat, item.lon]);
        dispatch(
          getForecastData({
            lat: item.lat,
            lon: item.lon,
            unit,
          })
        );
        dispatch(
          getPolutionData({
            lat: item.lat,
            lon: item.lon,
            unit,
          })
        );
      } else {
        console.error('Failed to fetch data');
      }
      setLoadings(false);
    }).catch((error) => {
      console.error('An error occurred while fetching data:', error);
      setIsError(true);
    });
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoadings(true);
    fetchData();
  };

  const isLoading = citySearchLoading || forecastLoading;

  return (
    <main>
      {isError ? (
        <button className="bg-white"
          onClick={() => window.location.href = '/weather'}
        >Go to home
        </button>
      ) : isLoading ? (
        <div className="flex justify-center items-center h-screen">
          <SphereSpinner />
        </div>
      ) : (
        <div className="w-full flex flex-col justify-center items-center">
          <div className="container h-full">
            {/* Display spinner while data is loading */}
            <div className="flex gap-x-8 pt-10">
              <div className="w-[30%] flex flex-col bg-[#262626] rounded-2xl">
                <div className="flex flex-col justify-center">
                  <form autoComplete="off" onSubmit={handleSubmit}>
                    <div className="flex justify-center items-center my-0 py-5 relative">
                      <input
                        className="border-none focus:outline-none w-[90%] p-[10px]
                        rounded-3xl pl-12 text-slate-200 bg-[#141414]"
                        type="text"
                        placeholder="Search city..."
                        onChange={(e) => setCity(e.target.value)}
                        value={city}
                        disabled={isLoading} // Disable input while loading
                      />
                      <button
                        className="absolute top-6 left-5 mt-1 ml-2 rounded-2xl"
                        type="submit"
                        disabled={isLoading} // Disable button while loading
                      >
                        <Image
                          src="/images/search.png"
                          className="w-[30px] h-[30px] rounded-3xl"
                          width={30}
                          height={30}
                          alt="search"
                        />
                      </button>
                    </div>
                  </form>
                  <WeatherMain />
                </div>
                <div className="flex flex-col h-[61vh] w-full justify-end gap-y-2">
                  <h1 className="flex items-center pl-4 font-bold text-[18px]">
                    Your location
                  </h1>
                  <Map position={position} />
                </div>
              </div >

              {/* Forecast and highlight section */}
              < div className="flex flex-col w-[70%] justify-between" >
                {/* Weather forecast */}
                < WeatherForecast />
                {/* Today's highlights */}
                <div div className="flex flex-col h-[70vh] w-full justify-center px-5 rounded-2xl bg-[#262626]" >
                  <div className="flex flex-col">
                    <h1 className="text-[18px] mb-4 font-bold">
                      Today's highlights
                    </h1>
                    <div className="grid grid-cols-2 h-[400px] gap-5">
                      {/* Air quality */}
                      <WeatherPollutions />
                      {/* Other weather details */}
                      <WeatherDetails />
                    </div>
                  </div>
                </div>
              </div >
            </div >
          </div >
        </div >
      )}
    </main >
  );
}
