import Link from "next/link";
import Image from "next/image";
import "./layout.css";

const NotFoundPage = () => {
  return (
    <div className="flex flex-col items-center justify-center h-screen bg-[#DEEDF7]">
      <h1 className="heading1">Oops! Something went wrong</h1>
      <p className="heading2 mb-8">
        Trang của bạn tìm kiếm không tồn tại, vui lòng thử lại
      </p>
      <Image
        src="/images/404bg.png"
        className="w-[400px] h-[400px]"
        alt="notfound"
        width={400}
        height={400}
      />
      <Link href="/map">
        <button className="flex justify-center items-center rounded-[10px] bg-slate-500 px-[30px] py-[15px] hover:bg-primary gap-x-2.5 my-12">
          <span className="heading3">Quay về trang chủ</span>
        </button>
      </Link>
    </div>
  );
};

export default NotFoundPage;
