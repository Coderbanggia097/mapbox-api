import Instructions from "./instruction";
const Navigation = ({ steps }) => {
    return (
        <div className="absolute top-0 left-4 z-10 p-[20px] flex flex-col">
            <div className="bg-slate-900 flex flex-col justify-center items-center p-3 rounded-t-lg">
                <h3 className="font-serif text-[20px] font-bold">Navigation here</h3>
            </div>

            <article
                className="bg-slate-800 max-w-xs overflow-y-auto p-2 rounded-b-lg"
                style={{ maxHeight: "calc(100vh - 8rem)" }}
            >
                {steps.map((item, idx) => (
                    <div key={idx} className="flex flex-col gap-2">
                        <Instructions
                            steps={item.maneuver.instruction}
                            number={idx + 1}
                        />
                    </div>
                ))}
            </article>
        </div>
    );
}

export default Navigation;