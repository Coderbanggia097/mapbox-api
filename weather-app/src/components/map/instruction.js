  import React from "react";

const Instructions = ({ steps, number }) => {
  return (
    <article className="flex justify-start items-center gap-2">
      <div className="text-slate-400">
        {number} - <b className="text-slate-400 font-semibold">{steps}</b>
      </div>
    </article>
  );
};

export default Instructions;
