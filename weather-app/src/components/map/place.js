import react, { useEffect, useState } from "react";
import { FaSearch, FaSortDown, FaSortUp } from "react-icons/fa";
import { MdErrorOutline } from "react-icons/md";
import { SphereSpinner } from "react-spinners-kit";

const Places = ({ setEnd }) => {

    const [place, setPlace] = useState([]);
    const [value, setValue] = useState("");
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        getPlaces();
    }, [value]);

    const getPlaces = async () => {
        setLoading(true);
        const response = await fetch(`https://api.mapbox.com/geocoding/v5/mapbox.places/${value}.json?access_token=${process.env.NEXT_PUBLIC_MAPBOX_ACCESS_TOKEN}`);
        const data = await response.json();
        setPlace(data.features);
        setLoading(false);
    }

    const handleClick = (item) => {
        setEnd(item.geometry.coordinates);
    }

    const handleClear = () => {
        setValue("");
    }

    return (
        <div className="top-5 right-10">
            <div className="rounded-lg">
                <div className="flex w-full items-center justify-center px-3 bg-white">
                    <div className="text-black"><FaSearch size={20} /></div>
                    <input className="w-full px-4 py-2 outline-none text-black"
                        placeholder="Enter destination"
                        value={value}
                        onChange={(e) => setValue(e.target.value)}>
                    </input>
                    {loading ? (<SphereSpinner size={20} color="black" />) : (
                        value && <div className="text-black p-1 hover:bg-slate-400 rounded-2xl"
                            onClick={handleClear}>
                            <FaSortUp size={20} />
                        </div>
                    )}
                </div>
                <div className="max-w-sm">
                    {place.length > 0 ? (
                        place?.map((item, idx) => (
                            <div key={idx} onClick={() => handleClick(item)}
                                className="flex flex-row items-center justify-between
                                    gap-2 bg-white hover:bg-gray-400 cursor-pointer shadow-md p-4">
                                <h4 className="text-cut text-[0.8rem] text-black">{item.text}</h4>
                                <p className="text-cut text-[0.8rem] text-black font-bold">{item.place_name}</p>
                            </div>
                        ))
                    ) : (
                        !loading && value !== "" && <div className="flex flex-row items-center justify-between
                        gap-2 bg-white hover:bg-gray-400 cursor-pointer shadow-md p-4">
                            <h4 className="text-cut text-[15px] text-black">Not Found</h4>
                            <MdErrorOutline size={22} className="text-black" />
                        </div>
                    )}
                </div>
            </div>
        </div >
    )
}

export default Places;