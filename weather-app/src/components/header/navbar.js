"use client";

import Link from "next/link";
import Image from "next/image";
import { useState } from "react";

export default function Header() {
  return (
    <div className="flex flex-col">
      <div className="flex justify-center bg-primary">
        <div className="container">
          <div className="flex justify-between items-center px-[2px] pt-[22px] md:px-[10px]">
            <Link href="/" className="md:flex">
              <Image
                src="/images/logo.png"
                width={200}
                height={50}
                className="w-[200px] h-[50px] object-contain "
                alt="weather-app"
              />
            </Link>

            <Link
              href="/"
              className="flex items-center bg-orange rounded-[10px]"
            ></Link>
          </div>
        </div>
      </div>
    </div>
  );
}
