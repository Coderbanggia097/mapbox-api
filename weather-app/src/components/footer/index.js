"use client";
import Image from "next/image";

export default function Footer() {
  return (
    <div className="flex flex-col w-full mt-10 px-10">
      <div className="flex justify-center bg-primary">
        <div className="container px-[27px] md:px-[2px]">
          <footer className="flex flex-col text-white md:items-stretch sm:items-center">
            <div
              className="flex flex-col items-start sm:flex-col gap-10
                text-center pt-2 text-white text-sm pb-8 mt-5 bg-primary"
            >
              <span className="heading5 flex justify-start">
                © Copyright 2024 DaoNM. All Rights Reserved
              </span>
            </div>
          </footer>
        </div>
      </div>
    </div>
  );
}
