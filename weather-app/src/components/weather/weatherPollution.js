import { useSelector } from "react-redux";
import Image from "next/image";

const WeatherPollutions = () => {
  const { polutionData } = useSelector((state) => state.weather);

  return (
    <>
      {polutionData && polutionData.data ? (
        <>
          <div className="flex flex-col bg-[#141414] justify-start items-start rounded-xl p-5">
            <h1 className="font-extralight">Air Quality Index</h1>
            <div className="flex flex-row gap-6 my-8 items-center justify-center">
              <Image
                src="/images/wind.png"
                className="w-[45px] h-[45px] mt-2"
                width={45}
                height={45}
                alt="wind"
              />
              <div className="flex flex-col items-start">
                <h1 className="text-[15px]">PM25</h1>
                <p className="text-[23px]">
                  {polutionData ? (
                    <>{polutionData.data.list[0].components.pm2_5}</>
                  ) : (
                    <div>Not found</div>
                  )}
                </p>
              </div>
              <div className="flex flex-col items-start">
                <h1>SO2</h1>
                <p className="text-[23px]">
                  {polutionData ? (
                    <>{polutionData.data.list[0].components.so2}</>
                  ) : (
                    <div>Not found</div>
                  )}
                </p>
              </div>
              <div className="flex flex-col items-start">
                <h1>NO2</h1>
                <p className="text-[23px]">
                  {polutionData ? (
                    <>{polutionData.data.list[0].components.no2}</>
                  ) : (
                    <div>Not found</div>
                  )}
                </p>
              </div>
              <div className="flex flex-col items-start">
                <h1>O3</h1>
                <p className="text-[23px]">
                  {polutionData ? (
                    <>{polutionData.data.list[0].components.o3}</>
                  ) : (
                    <div>Not found</div>
                  )}
                </p>
              </div>
            </div>
          </div>
        </>
      ) : (
        <></>
      )}
    </>
  );
};

export default WeatherPollutions;
