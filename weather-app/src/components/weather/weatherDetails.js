import { useSelector } from "react-redux";
import Image from "next/image";
import { getTime } from "./data";

const WeatherDetails = () => {
  const { citySearchData } = useSelector((state) => state.weather);

  const convertVisibility = (visMS) => {
    const visKM = (visMS * 0.001).toFixed(0);
    return visKM;
  };

  return (
    <>
      {citySearchData && citySearchData.data ? (
        <>
          <div className="flex flex-col bg-[#141414] justify-start items-start rounded-xl p-5">
            <h1 className="font-extralight">Sunshine and sunset</h1>
            <div className="flex flex-row my-8 w-full justify-between">
              <div className="flex flex-row gap-3 justify-center items-center">
                <Image
                  src="/images/sunny-day.png"
                  className="w-[45px] h-[45px]"
                  width={45}
                  height={45}
                  alt="sunny"
                />
                <div className="flex flex-col justify-center items-start">
                  <h1 className="text-[16px]">Sunrise</h1>
                  <p className="text-[26px]">
                    {getTime(
                      citySearchData.data.sys.sunrise,
                      citySearchData.data.timezone
                    )}
                  </p>
                </div>
              </div>
              <div className="flex flex-row gap-3 items-center">
                <Image
                  src="/images/moon.png"
                  className="w-[45px] h-[45px]"
                  width={45}
                  height={45}
                  alt="moon"
                />
                <div className="flex flex-col justify-center items-start">
                  <h1 className="text-[16px]">Sunset</h1>
                  <p className="text-[26px]">
                    {getTime(
                      citySearchData.data.sys.sunset,
                      citySearchData.data.timezone
                    )}
                    {/* {2 > 1 ? "b" : null}
                    {2 > 1 && "b"} */}
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div className="grid grid-cols-2 gap-x-4">
            <div className="flex flex-col bg-[#141414] justify-start items-start rounded-xl p-5">
              <h1 className="font-extralight">Humidity</h1>
              <div className="flex flex-row gap-12 mt-10 items-center">
                <Image
                  src="/images/night.png"
                  className="w-[45px] h-[45px]"
                  width={45}
                  height={45}
                  alt="night"
                />
                <div className="flex flex-col">
                  <p className="text-[23px]">
                    {citySearchData.data.main.humidity}%
                  </p>
                </div>
              </div>
            </div>

            <div className="flex flex-col bg-[#141414] justify-start items-start rounded-xl p-5">
              <h1 className="font-extralight">Pressure</h1>
              <div className="flex flex-row gap-6 mt-10 items-center">
                <Image
                  src="/images/pressure.png"
                  className="w-[45px] h-[45px]"
                  width={45}
                  height={45}
                  alt="pressure"
                />
                <div className="flex flex-col">
                  <p className="text-[23px]">
                    {citySearchData.data.main.pressure}hPa
                  </p>
                </div>
              </div>
            </div>
          </div>

          <div className="grid grid-cols-2 gap-x-4">
            <div className="flex flex-col bg-[#141414] justify-start items-start rounded-xl p-5">
              <h1 className="font-extralight">Visibility</h1>
              <div className="flex flex-row gap-10 mt-10 items-center">
                <Image
                  src="/images/visibility.png"
                  className="w-[50px] h-[50px]"
                  width={50}
                  height={50}
                  alt="visibility"
                />
                <div className="flex">
                  <p className="text-[25px]">
                    {convertVisibility(citySearchData.data.visibility)}km
                  </p>
                </div>
              </div>
            </div>

            <div className="flex flex-col bg-[#141414] justify-start items-start rounded-xl p-5">
              <h1 className="font-extralight">Feels Like</h1>
              <div className="flex flex-row items-center gap-12 mt-10">
                <Image
                  src="/images/feels.png"
                  className="w-[45px] h-[45px]"
                  width={45}
                  height={45}
                  alt="feelslike"
                />
                <div className="">
                  <p className="text-[23px]">
                    {Math.round(citySearchData.data.main.feels_like)}°C
                  </p>
                </div>
              </div>
            </div>
          </div>
        </>
      ) : (
        <>
          <div className="flex flex-col bg-[#141414] justify-start items-start rounded-xl p-5">
            <h1 className="font-extralight">Sunshine and sunset</h1>
            <div className="flex flex-row my-8 w-full justify-between">
              <div className="flex flex-row gap-3 justify-center items-center">
                <Image
                  src="/images/sunny-day.png"
                  className="w-[45px] h-[45px]"
                  width={45}
                  height={45}
                  alt="sunny"
                />
                <div className="flex flex-col justify-center items-start">
                  <h1 className="text-[12px]">Sunrise</h1>
                  <p className="text-[27px]">null</p>
                </div>
              </div>
              <div className="flex flex-row gap-3 items-center">
                <Image
                  src="/images/moon.png"
                  className="w-[45px] h-[45px]"
                  width={45}
                  height={45}
                  alt="moon"
                />
                <div className="flex flex-col justify-center items-start">
                  <h1 className="text-[12px]">Sunset</h1>
                  <p className="text-[27px]">null</p>
                </div>
              </div>
            </div>
          </div>

          <div className="grid grid-cols-2 gap-x-4">
            <div className="flex flex-col bg-[#141414] justify-start items-start rounded-xl p-5">
              <h1 className="font-extralight">Humidity</h1>
              <div className="flex flex-row gap-12 mt-10 items-center">
                <Image
                  src="/images/night.png"
                  className="w-[45px] h-[45px]"
                  width={45}
                  height={45}
                  alt="night"
                />
                <div className="flex flex-col">
                  <p className="text-[23px]">null</p>
                </div>
              </div>
            </div>

            <div className="flex flex-col bg-[#141414] justify-start items-start rounded-xl p-5">
              <h1 className="font-extralight">Pressure</h1>
              <div className="flex flex-row gap-6 mt-10 items-center">
                <Image
                  src="/images/pressure.png"
                  className="w-[45px] h-[45px]"
                  width={45}
                  height={45}
                  alt="pressure"
                />
                <div className="flex flex-col">
                  <p className="text-[23px]">null</p>
                </div>
              </div>
            </div>
          </div>

          <div className="grid grid-cols-2 gap-x-4">
            <div className="flex flex-col bg-[#141414] justify-start items-start rounded-xl p-5">
              <h1 className="font-extralight">Visibility</h1>
              <div className="flex flex-row gap-10 mt-10 items-center">
                <Image
                  src="/images/visibility.png"
                  className="w-[50px] h-[50px]"
                  width={50}
                  height={50}
                  alt="visibility"
                />
                <div className="flex">
                  <p className="text-[25px]">null</p>
                </div>
              </div>
            </div>

            <div className="flex flex-col bg-[#141414] justify-start items-start rounded-xl p-5">
              <h1 className="font-extralight">Feels Like</h1>
              <div className="flex flex-row items-center gap-12 mt-10">
                <Image
                  src="/images/feels.png"
                  className="w-[45px] h-[45px]"
                  width={45}
                  height={45}
                  alt="feelslike"
                />
                <div className="">
                  <p className="text-[23px]">null</p>
                </div>
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};

export default WeatherDetails;
