import { useSelector } from "react-redux";
import Image from "next/image";
import {
  weekDayNames,
  monthNames,
  getDateMonth,
  getTime,
} from "@/components/weather/data";
import { useEffect, useState } from "react";

const WeatherMain = () => {
  const { citySearchData } = useSelector((state) => state.weather);

  const upWeather = (string) => {
    const sentence = string.toLowerCase().split(" ");
    for (var i = 0; i < sentence.length; i++) {
      sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
    }
    return sentence.join(" ");
  };

  return (
    <>
      {citySearchData && citySearchData.data ? (
        <>
          <div className="flex flex-row justify-between items-center space-x-14 border-b border-gray-600 mx-5 py-5 mb-4">
            <div className="flex flex-col">
              <div className="info justify-center items-start gap-y-1">
                <p className="details text-[50px]">
                  {Math.round(citySearchData.data.main.temp)}°C
                </p>
                <p className="heading1 text-[20px]">
                  {upWeather(citySearchData.data.weather[0].description)}
                </p>
              </div>
            </div>
            <Image
              src={`http://openweathermap.org/img/wn/${citySearchData.data.weather[0].icon}@2x.png`}
              className="w-[100px] h-[100px] object-contain"
              width={100}
              height={100}
              alt="temperature"
            />
          </div>
          <div className="flex flex-col justify-center items-start ml-5 gap-y-2">
            <div className="flex flex-row items-center gap-x-3">
              <Image
                src="/images/calendar.png"
                className="w-[20px] h-[20px] object-contain"
                width={20}
                height={20}
                alt="temperature"
              />
              <p className="font-extralight">
                {getDateMonth(
                  citySearchData.data.dt,
                  citySearchData.data.timezone
                )}
              </p>
            </div>
            <div className="flex flex-row items-center gap-x-3">
              <Image
                src="/images/location.png"
                className="w-[20px] h-[20px] object-contain"
                width={20}
                height={20}
                alt="temperature"
              />
              <p className="font-extralight">{citySearchData.data.name}</p>
            </div>
          </div>
        </>
      ) : (
        <></>
      )}
    </>
  );
};

export default WeatherMain;
