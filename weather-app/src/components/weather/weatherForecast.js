import { useSelector } from "react-redux";
import Image from "next/image";
import { weekDayNames, monthNames } from "@/components/weather/data";
import next from "next";

const WeatherForecast = () => {
  const { forecastData } = useSelector((state) => state.weather);

  const filterForecastByFirstObjTime = (forecastData) => {
    if (!forecastData) {
      return [];
    }

    const firstObjTime = forecastData[0].dt_txt.split(" ")[1];
    return forecastData.filter((data) => data.dt_txt.endsWith(firstObjTime));
  };

  const filteredForecast = filterForecastByFirstObjTime(forecastData?.list);
  console.log(filteredForecast);

  const upWeather = (string) => {
    const sentence = string.toLowerCase().split(" ");
    for (var i = 0; i < sentence.length; i++) {
      sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
    }
    return sentence.join(" ");
  };

  return (
    <>
      {filteredForecast.length > 0 ? (
        <>
          <div className="flex flex-col gap-y-3">
            <h1 className="text-[20px] font-bold mb-3">5 Days Forecast</h1>
            <div className="grid grid-cols-5 h-[160px] justify-center gap-x-5">
              {filteredForecast.map((data, value) => {
                const date = new Date(data.dt_txt);
                const day = date.toLocaleDateString("en-US", {
                  weekday: "short",
                });
                return (
                  <>
                    <div
                      key={value}
                      className="flex flex-col justify-center items-center bg-[#262626] rounded-xl"
                    >
                      <p>{day}</p>
                      <Image
                        src={`http://openweathermap.org/img/wn/${data.weather[0].icon}.png`}
                        className="w-[50px] h-[50px]"
                        width={50}
                        height={50}
                        alt="temperature"
                      />
                      <p className="font-extralight">
                        {upWeather(data.weather[0].description)}
                      </p>
                      <p className="font-light">
                        {Math.round(data.main.temp_min)}° -{" "}
                        {Math.round(data.main.temp_max)}°
                      </p>
                    </div>
                  </>
                );
              })}
            </div>
          </div>
        </>
      ) : (
        <div>No Data Found</div>
      )}
    </>
  );
};

export default WeatherForecast;
