// ranging from 0 to 6 (0 is Sunday)
const weekDayNames = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
];

const monthNames = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "July",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec",
];

const weatherDetails = ["Humidity", "Pressure", "Visibility", "Feels Like"];

const getDate = (unix, timezone) => {
  return new Date((unix + timezone) * 1000);
};

const getDateMonth = (dateUnix, timezone) => {
  const date = getDate(dateUnix, timezone);
  const month = date.getUTCDate();
  const weekDayName = weekDayNames[date.getUTCDay()];
  const monthName = monthNames[date.getUTCMonth()];
  return `${weekDayName} ${month}, ${monthName}`;
};

const getTime = (timeUnix, timezone) => {
  const date = getDate(timeUnix, timezone);
  const hours = date.getUTCHours();
  const minutes = date.getUTCMinutes();
  const period = hours >= 12 ? "PM" : "AM";

  const formattedHours = hours % 12 || 12;
  const formattedMinutes = minutes.toString().padStart(2, 0);

  return `${formattedHours}:${formattedMinutes} ${period}`;
};

const getLocalTime = (dt, timezone) => {
  const timestampMs = dt * 1000;
  const utcDate = new Date(timestampMs);
  const utcTime = utcDate.getTime();
  const localOffsetSeconds = timezone;
  const localTimeMs = utcTime + localOffsetSeconds * 1000;
  const localDate = new Date(localTimeMs);
  const hours = localDate.getHours();
  const minutes = localDate.getMinutes();
  const seconds = localDate.getSeconds();
  const period = hours >= 12 ? "PM" : "AM";
  const formattedHours = hours % 12 || 12;
  const formattedMinutes = minutes.toString().padStart(2, "0");
  const formattedSeconds = seconds.toString().padStart(2, "0");

  return `${formattedHours}:${formattedMinutes}:${formattedSeconds} ${period}`;
};

const mps_to_kmh = (mps) => {
  const mph = mps * 3600;
  return mph / 1000;
};

export {
  weekDayNames,
  monthNames,
  getDateMonth,
  getTime,
  getLocalTime,
  mps_to_kmh,
  weatherDetails,
};
