import { apiId, hostName } from "@/config/config";
import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import axios from "axios";

export const getCurrentData = createAsyncThunk("city", async (obj) => {
  try {
    const req = await axios.get(
      `${hostName}/weather?q=${obj.city}&units=${obj.unit}&appid=${apiId}`
    );
    const res = (await req).data;
    return {
      data: res,
      error: null,
    };
  } catch (error) {
    return {
      data: null,
      error: error.res.data.message,
    };
  }
});

export const getForecastData = createAsyncThunk("forecast", async (obj) => {
  const req = await axios.get(
    `${hostName}/forecast?lat=${obj.lat}&lon=${obj.lon}&units=${obj.unit}&appid=${apiId}`
  );
  const res = (await req).data;
  return res;
});

export const getPolutionData = createAsyncThunk("polution", async (obj) => {
  try {
    const req = await axios.get(
      `${hostName}/air_pollution?lat=${obj.lat}&lon=${obj.lon}&units=${obj.unit}&appid=${apiId}`
    );
    const res = await req.data;
    return {
      data: res,
      error: null,
    };
  } catch (error) {
    return {
      data: null,
      error: error.res.data.message,
    };
  }
});

const weatherSlice = createSlice({
  name: "weather",
  initialState: {
    citySearchLoading: false,
    citySearchData: null,
    forecastLoading: false,
    forecastData: null,
    forecastError: null,
    polutionLoading: false,
    polutionData: null,
  },
  reducers: {},
  extraReducers(builder) {
    builder.addCase(getCurrentData.pending, (state, action) => {
      state.citySearchLoading = true;
      state.citySearchData = null;
    });
    builder.addCase(getCurrentData.fulfilled, (state, action) => {
      state.citySearchLoading = false;
      state.citySearchData = action.payload;
    });
    // builder.addCase(getCurrentData.rejected, (state, action) => {
    //   state.citySearchLoading = false;
    //   state.citySearchData = null;
    //   state.citySearchError = action.error.message;
    // });

    // Forecast
    builder.addCase(getForecastData.pending, (state, action) => {
      state.forecastLoading = true;
      state.forecastData = null;
      state.forecastError = null;
    });
    builder.addCase(getForecastData.fulfilled, (state, action) => {
      state.forecastLoading = false;
      state.forecastData = action.payload;
      state.forecastError = null;
    });
    builder.addCase(getForecastData.rejected, (state, action) => {
      state.forecastLoading = false;
      state.forecastData = null;
      state.forecastError = action.error.message;
    });
    // Air Polution
    builder.addCase(getPolutionData.pending, (state) => {
      state.polutionLoading = true;
      state.polutionData = null;
    });
    builder.addCase(getPolutionData.fulfilled, (state, action) => {
      state.polutionLoading = false;
      state.polutionData = action.payload;
    });
    builder.addCase(getPolutionData.rejected, (state) => {
      state.polutionLoading = false;
      state.polutionData = null;
    });
  },
});

export default weatherSlice.reducer;
