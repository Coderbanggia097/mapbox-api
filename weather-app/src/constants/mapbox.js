export const lineStyle = {
    id: "roadLayer",
    type: "line",
    layout: {
        "line-join": "round",
        "line-cap": "round",
    },
    paint: {
        "line-color": "blue",
        "line-width": 4,
        "line-opacity": 0.75,
    },
};

export function geojson(coords) {
    return {
        type: "FeatureCollection",
        features: [
            {
                type: "Feature",
                geometry: {
                    type: "LineString",
                    coordinates: coords,
                },
            },
        ],
    }
};

export function endPoint(endP) {
    return {
        type: "FeatureCollection",
        features: [
            {
                type: "Feature",
                geometry: {
                    type: "Point",
                    coordinates: [...endP],
                },
            },
        ],
    }
};

export function endPointStyle(endP) {
    return {
        id: "end",
        type: "circle",
        source: {
            type: "geojson",
            data: endP,
        },
        paint: {
            "circle-radius": 5,
            "circle-color": "blue",
        },
    }
}

export default { lineStyle, geojson, endPoint, endPointStyle }; 