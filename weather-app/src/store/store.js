import weatherReducer from "@/features/WeatherSlice";
import { configureStore } from "@reduxjs/toolkit";

export default configureStore({
  reducer: {
    weather: weatherReducer,
  },
});
